import React from 'react';
import {Button, Input} from "antd";

function Navbar() {
    return (
        <div className="navbar">
            <div className="navbarTop">
                <div className="container">
                   <div className="navbarTopContent">
                       <div className="navbar-brand">
                           <img src="images/Dustlikdon.uz.svg" alt="no images"/>
                       </div>
                       <div className="inputSearch">
                           <Input  placeholder={"Saytdan qidirish"} type={"text"} className={"form-control"}/>
                       </div>
                       <div className="telephone">
                           <Button>
                               <img src="images/Vector (11).svg" alt="no images"/>
                               998 (99) 400-04-40
                           </Button>
                       </div>
                   </div>
                </div>
            </div>
            <div className="navbarBottom">
               <div className="container">
                   <div className="navbarBottomContent">
                       <div className="homeButton">
                           <img src="images/Frame.svg" alt="no images"/>
                       </div>
                       <a href="#" className='nav-link'>
                           Jamiyat haqida
                       </a>
                       <a href="#" className='nav-link'>
                           Struktura
                       </a>
                       <a href="#" className='nav-link'>
                           Yangiliklar
                       </a>
                       <a href="#" className='nav-link'>
                           Elektron murojatlar
                       </a>
                       <a href="#" className='nav-link'>
                           Interaktiv xizmatlar
                       </a>
                       <a href="#" className='nav-link'>
                           Aloqa
                       </a>
                   </div>
               </div>
            </div>
        </div>
    );
}

export default Navbar;