import React from 'react';
import "./admin.css";
import { Layout, Menu } from 'antd';
import {
    MenuUnfoldOutlined,
    MenuFoldOutlined,
    UserOutlined,
    VideoCameraOutlined,
    UploadOutlined,
} from '@ant-design/icons';
import {Link, Route, Switch} from "react-router-dom";
import NewsList from "./newsList";



function Admin() {
    const [collapsed, setCollapsed]=React.useState(false);
    function toggle(){
        setCollapsed(!collapsed);
    }


    const { Header, Sider, Content } = Layout;

    return (
        <div className="admin">
            <Layout>
                <Sider trigger={null} collapsible collapsed={collapsed}>
                    <div className="logo" />
                    <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
                        <Menu.Item key="1" icon={<UserOutlined />}>
                            <Link to="/admin/newsList" >NewsList</Link>
                        </Menu.Item>
                        <Menu.Item key="2" icon={<VideoCameraOutlined />}>
                            nav 2
                        </Menu.Item>
                        <Menu.Item key="3" icon={<UploadOutlined />}>
                            nav 3
                        </Menu.Item>
                    </Menu>
                </Sider>
                <Layout className="site-layout">
                    <Header className="site-layout-background" style={{ padding: 0 }}>
                        {React.createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                            className: 'trigger',
                            onClick:toggle,
                        })}
                    </Header>
                       <Switch>
                           <Route path="/admin/newsList"  component={NewsList}/>
                       </Switch>
                </Layout>
            </Layout>
        </div>
    );
}

export default Admin;